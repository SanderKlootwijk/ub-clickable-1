:doctype: book
:source-highlighter: coderay
:listing-caption: Listing
:pdf-page-size: A4
:toc:
:toc-placement: left
:toclevels: 4
:icons: font
:sectnums:
include::include/variablesvar.ad[] 
++++
<link rel="stylesheet"  href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/3.1.0/css/font-awesome.min.css">
<link rel="stylesheet"  href="include/our.css">
++++

include::include/header.ad[]
link:trainingpart1module2.html[Next {modulename}]


= {modulename} 1: [.titlemodule]#{module1Title}#

You'd like to develop an app for https://ubuntu-touch.io[Ubuntu Touch]? Great! In this module you'll learn how to set up your development environment for Ubuntu Touch apps on Windows, macOS or Linux. To confirm that you have a working setup, you'll run a simple Clickable app on your phone.

== Introduction 

https://clickable-ut.dev[Clickable] is the main tool to build apps on Ubuntu Touch. It's an easy to use command line tool with a lot of great features. Clickable allows you to compile, build, test, and publish your app, all with one tool. It also provides various templates to get you started with app development.

Clickable works on Linux, but if you're using Windows or macOS you can run Clickable in a virtual machine. In the next section you'll learn how to install Clickable on all three operating systems.

You can use any code editor or IDE (integrated development environment) to work on your app's code. In this course we're going to use Visual Studio Code. Then you build your app from the command line with Clickable. And with one command, you can run your app on your desktop or on your phone to test it.

When your app is ready, you can ship it as a click package and publish it on our official app store, https://open-store.io[OpenStore].

IMPORTANT: In the rest of this course, we suppose you have a working Ubuntu Touch device. If not, have a look at https://devices.ubuntu-touch.io[The list of supported Ubuntu Touch devices]. You can install Ubuntu Touch on supported Android devices with the https://devices.ubuntu-touch.io/installer/[UBports installer] or buy a https://volla.online/en/index.html[Volla Phone or Volla Phone X with Ubuntu Touch pre-installed].

== Installing Clickable

Clickable is a Linux tool, so we'll show you first how to install it on Linux. As an example, we're using the https://ubuntu.com[Ubuntu] Linux distribution. You can https://ubuntu.com/download/desktop[download] it for free and install it on your PC. In later subsections you'll see how to run Ubuntu as a virtual machine on Windows or macOS, so you can install and run Clickable in that virtual machine.

WARNING: All commands shown in this course are Linux commands. If you're using Windows or macOS, remember that you need to enter them in the Ubuntu machine on Windows or Mac.

=== Linux

Clickable is a Python package, so you need to install Python's package manager pip. You also need Docker to run your apps on your desktop, the Android Debug Bridge (adb) to run your apps on your phone, and we also recommend Git for version control of the apps you're building.

So open a terminal on your Linux machine and install all these requirements with this command:

----
sudo apt install docker.io adb git python3 python3-pip python3-setuptools
----

Now install the Python package for Clickable:

----
pip3 install --user clickable-ut
----

And then add the location of the command you've installed with pip to your PATH variable:

----
echo 'export PATH="$PATH:$HOME/.local/bin"' >> ~/.bashrc
----

Open a new terminal for this to take effect, and then let Clickable set up Docker and bash completion:

----
clickable setup
----

It's recommended to restart your Linux machine now.

=== Windows

Recent Windows versions have a great way to run Linux applications, https://docs.microsoft.com/en-us/windows/wsl/[WSL (Windows Subsystem for Linux)]. This requires Windows 10 version 2004 and higher (Build 19041 and higher) or Windows 11.

To install WSL, open an administrator PowerShell or Windows Command Prompt and enter the following command:

----
wsl --install
----

This command will enable the required optional components, download the latest Linux kernel, set WSL 2 as your default, and install Ubuntu. You will need to restart your Windows machine during this installation process.

Once the installation is complete, open Ubuntu using the Start menu. You'll be asked to create a user name and password. After this, you're logged in on your Ubuntu machine. Next times you open Ubuntu using the Start menu, you'll be automatically logged in with this account.

Now that you're working in your Ubuntu machine on Window, update the package database first:

----
sudo apt update
----

When asked, enter the password that you've chosen for your user account.

Then run the same commands as in the previous subsection ("Installing Clickable") to install all Clickable requirements and Clickable itself, and to add the location of the command to your PATH variable. Then log out of Ubuntu with Ctrl+d and open Ubuntu again using the Start menu. Skip the step to set up Docker and bash completion.

Now install https://docs.docker.com/docker-for-windows/wsl/[Docker Desktop for Windows], and log out from Windows and log in again.

TODO

=== macOS

If you're using a Mac, the best way to run Clickable is in a virtual machine running Ubuntu on VMware Fusion Player, which works on both Intel and Apple Silicon Macs. VMware offers a https://customerconnect.vmware.com/web/vmware/evalcenter?p=fusion-player-personal[Personal Use License], available for free. You only need to register a MyVMware account, after which you can use VMware Fusion Player for free for non-commercial use.

Once you're logged in on the web site, click on *Register* on the product's page. Then accept the end-user license agreement. You need to enter some personal details for this, and click on *Sign Up* at the bottom of the page.

On this page you'll find your license for personal use, which you will need later during the installation of VMware Fusion Player.

The installation process is a bit different on the new Apple Silicon Macs, compared to Intel Macs. Please follow the correct installation method for your Mac. This guide currently contains install instructions for Intel Macs only.

==== Intel Macs

To install VMware Fusion Player on Intel Macs, you'll need to download a copy from https://customerconnect.vmware.com/downloads/info/slug/desktop_end_user_computing/vmware_fusion/12_0[here]. Click on *Go to downloads*, then on *Download Now*. Login with your MyVMware account and download the DMG file.

Once Downloaded, open the DMG file and double-click the VMware Fusion icon to start the installation. You will have to enter your user password, to complete the installation. Follow the installer instructions. Once the installer has finished, VMware Fusion will show a screen where you can select an installation method. Now you are ready to install Ubuntu in a virtual machine. Download a copy of Ubuntu 20.04.4 LTS from https://ubuntu.com/download/desktop/thank-you?version=20.04.4&architecture=amd64[here] and drag the ISO on the VMware window, once the download is finished.

Click *Continue* and choose *Easy Install* for a quick and easy installation. This installation method automatically sets up all the needed drivers, so you'll be able to plug in your Ubuntu Touch device and deploy to it directly from the virtual machine. If you want, you can customize the virtual machine settings on the *Finish* page, by clicking *Customize Settings*. It is recommended that you give the virtual machine a decent amount of storage (at least 40GB), as clickable takes up quite some space when downloading Docker images. To expand the virtual machine hard disk, click on *Hard Disk (SCSI)* under your virtual machine's settings. Use the slider or the input field to expand the hard disk. After that, click on *Apply*. When ready, click *Finish* if you didn't change any preset settings or close the virtual machine settings and run the virtual machine by clicking on the start icon. Once the installation of Ubuntu has finished, you will need to login and then will end up on the Ubuntu desktop. If the screen resolution isn't automaticaly set correctly, you can change this in the Ubuntu *Settings* app. Before installing Clickable, open a terminal window and use the following command to update the package lists:

----
sudo apt update
----

Now you can follow the installation instructions for Clickable on Linux, under chapter 2.1 in this guide.

==== Deploying to Ubuntu Touch from VMware

When using Clickable for deploying an app to your device, connect your device to your Mac with an USB cable. Select *Connect to VMware* to connect your Ubuntu Touch device to the virtual machine. When you run clickable, the app now will be deployed to your Ubuntu Touch device.

== Hello World!

To check that your development environment is set up correctly for Clickable, create a new Clickable project:

----
$ clickable create
Select Template:
1 - QML Only
2 - C++
3 - Python
4 - HTML
5 - Go
6 - Rust
7 - Godot (Precompiled)
Choose from 1, 2, 3, 4, 5, 6, 7 [1]: 
Title [App Title]: 
Description [A short description of your app]: 
App Name [appname]: 
Namespace [yourname]: 
Maintainer Name [Your FullName]: 
Maintainer Email [email@domain.org]: 
Select License:
1 - GNU General Public License v3
2 - MIT license
3 - BSD license
4 - ISC license
5 - Apache Software License 2.0
6 - Not open source
Choose from 1, 2, 3, 4, 5, 6 [1]: 
Copyright Year [2022]: 
Git Tag Versioning [n]: 
Save as Default [n]: 

Successfully created app "appname.yourname"
Get started with the following commands:

 $ cd appname
 $ clickable
----

This will ask you a couple of questions to configure your project. For now, just accept the default answers by pressing Enter on each question. This creates an app *appname.yourname* in the directory *appname*.

=== On your desktop

As Clickable helpfully tells you, you can start the app on your desktop with the following commands:

----
cd appname
clickable desktop
----

This builds your Clickable app for your computer's processor architecture and starts the app, displaying a window with a title and a "Hello World!" message:

image:images/trainingpart1module1/clickable-desktop.png[Clickable allows you to run and test the "Hello world" app on your desktop.]

Exit the app by clicking on the window's close icon.

=== On your Ubuntu Touch phone

The next step is to run this app on your Ubuntu Touch phone. Clickable is able to connect to your phone in two ways: using ADB (with a USB cable) or using SSH.

==== Using ADB

For ADB you need to enable developer mode on your Ubuntu Touch device. Open the Settings app, tap on *About*, then *Developer mode* and turn on the *Developer Mode* switch:

image:images/trainingpart1module1/developer-mode.png[Turn on Developer Mode on your Ubuntu Touch phone.]

Then plug your phone into a USB port of your computer. Open a terminal and run the following command:

----
$ adb devices
List of devices attached
GX290PRO0007222 device
----

If you only see "List of devices attached" with an empty line, this means that ADB doesn't recognize your device. In that case, type the following command and look at the list of your USB devices:

----
$ lsusb
Bus 002 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub
Bus 001 Device 002: ID 04f2:b684 Chicony Electronics Co., Ltd Chicony USB2.0 Camera
Bus 001 Device 004: ID 18d1:0000 Google Inc. Volla
Bus 001 Device 003: ID 8087:0029 Intel Corp. 
Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub
----

One of these devices is your phone. In this case, the line with "Google Inc. Volla" shows that a Volla Phone is connected. Take note of the hexadecimal numbers before the name: 18d1 is the vendor ID, while 0000 is the product ID. We need the vendor ID to make ADB recognize the device.

Now update the file *~/.android/adb_usb.ini* with this vendor ID:

----
echo 0x18d1 >> ~/.android/adb_usb.ini
----

Terminate the ADB server and try again to list the recognized devices:

----
adb kill-server
adb devices
----

Now that your device is recognized, run the following command to build the app for your phone and run it on your device:

----
clickable
----

The app window now appears on your phone:

image:images/trainingpart1module1/helloworld-device.png[The "Hello world" app runs on your Ubuntu Touch phone.]

Just swipe it away to close it.

TIP: Don't forget to disable developer mode after you're done with testing your app.

==== Using SSH

If you don't want to use a USB cable while developing your app and testing it on your phone, an alternative connection method to ADB is SSH (Secure SHell). This allows Clickable to connect to your phone over Wi-Fi, using an encrypted link.

You need to create an SSH key pair, because Ubuntu Touch disables logging in with a password by default. If you already have a key pair, skip the following command, otherwise run this on your Ubuntu machine to create this key pair:

----
ssh-keygen
----

The default choice for the file location is fine. You should enter a passphrase that is easy for you to remember and type, but not too easy for others to guess.

Then you need to transfer the public key part of this key pair to your phone. The easiest way to do this is using ADB, if your phone is still connected to USB and has the developer mode enabled:

----
adb push ~/.ssh/id_rsa.pub /home/phablet/
----

Now open a shell on your phone. You can do this by opening the Terminal app, which asks you to enter your phone's passcode or passphrase. Or you can enter the following command on your computer to open a shell with ADB:

----
adb shell
----

Check whether you already have a configuration directory for SSH:

----
ls .ssh
----

If not, create one and give it the right permissions:

----
mkdir /home/phablet/.ssh
chmod 700 /home/phablet/.ssh
----

Now add your public key to the list of authorized keys that are allowed to log in and give the file the right permissions:

----
cat /home/phablet/id_rsa.pub >> /home/phablet/.ssh/authorized_keys
chmod 600 /home/phablet/.ssh/authorized_keys
----

After this, start the SSH server. If you're using an Android-based device, this goes like this:

----
sudo android-gadget-service enable ssh
----

Is *sudo* asks for your password, enter your phone's passcode or passphrase.

If you are using a Linux-based device such as the PinePhone, run this instead:

----
sudo service ssh start
----

Now check your phone's IP address:

----
ip address show wlan0
----

To make Clickable on your computer use SSH to connect to your phone, run it with the following option and use your phone's IP address:

----
clickable --ssh 192.168.0.236
----

When connecting over SSH, your computer asks you to enter the passphrase of your key pair. After this, the app window appears on your phone. Just swipe it away to close it.

== Installing Visual Studio Code

The next step is to install a code editor or development environment. In this course we'll be using https://code.visualstudio.com/[Microsoft Visual Studio Code], which is free and runs on Windows, macOS and Linux. Just download it from its web site and install it.

If you're still in the directory of the default app, just run the following command to open the directory with Visual Studio Code:

----
code .
----

Visual Studio Code asks you if you trust the authors of the files in this directory. Confirm with a click on "Yes, I trust the authors".

At the left, all files in the app directory are listed. Just click on a directory to reveal its contents and click on a file to open it in the editor window at the right.

image:images/trainingpart1module1/visual-studio-code.png[View and edit the "Hello world" app's code in Visual Studio Code.]

Visual Studio Code offers a lot of extensions for extra functionality. For this course, the https://marketplace.visualstudio.com/items?itemName=bbenoist.QML[QML extension by Baptist Benoist] is recommended, because it adds syntax coloring. Open *Extensions* (the icon with the four blocks) at the left bar of Visual Studio Code and then search for *QML*. Install the one by *Baptist BENOIST* by clicking on it and then clicking on the *Install* button:

image:images/trainingpart1module1/visual-studio-code-qml.png[Install the QML extension in Visual Studio Code.]

After this, you get syntax coloring in all QML code, which makes your code easier to read:

image:images/trainingpart1module1/visual-studio-code-syntax-coloring.png[With the QML extension, you get syntax coloring in Visual Studio Code.]

== Getting help

There are a lot of places where you can get some help for your Ubuntu Touch app development. These are some great ones to get started:

* the https://t.me/UbuntuAppDevEN[UBports UT App Dev] Telegram group
* the https://forums.ubports.com/category/35/app-development[UBports App Development] category in the UBports forum
* the https://docs.ubports.com/en/latest/appdev/index.html[App development] documentation of UBports

And of course, the next module in this course will help you take the next steps in your Ubuntu Touch app development career. We're going to adapt the "Hello world" app!
