# Ubuntu Touch app development course

This course teaches you [how to develop an app for Ubuntu Touch with Clickable](https://ubports.gitlab.io/teams/marketing/education/ub-clickable-1/).

## How to contribute

Contributions to this course are welcome. The source documents are written in [AsciiDoc](https://asciidoc.org).

You can generate the HTML documentation from the source documents with:

```shell
./scripts/generate_html.sh
```

Then open the index file in `public/index.html`.

## License

[Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)](https://creativecommons.org/licenses/by-sa/4.0/)

The course text has been written by Koen Vervloesem, based on input and [code](https://github.com/codefounders-nl/ubtouch-shoppinglist) by Felix van Loenen, Terence Sambo, Leandro d'Agostino and Sander Klootwijk.
